import { Action } from '@ngrx/store';

export function reducer(state: number = 1, action: Action) {
  console.log(action.type, state);

  switch (action.type) {
    case 'CLICK':
      return (state = ++state);
    case 'TIME':
      return (state = state * 2);
    default:
      return state;
  }
}
