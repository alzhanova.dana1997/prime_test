import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { interval, Observable, Subscription } from 'rxjs';

interface AppState {
  text: number;
}

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss'],
})
export class TestPageComponent implements OnInit {
  text$: Observable<number>;
  subscription = new Subscription();
  form: FormGroup;

  constructor(
    private store: Store<AppState>,
    private formBuilder: FormBuilder
  ) {
    this.text$ = this.store.select('text');
  }

  ngOnInit(): void {
    const source = interval(10000);
    this.subscription = source.subscribe((val) => this.onSetTime());
    const savedForm = JSON.parse(localStorage.getItem('form') || '{}');
    this.form = this.formBuilder.group({
      name: [savedForm.name || null],
      sirname: [savedForm.sirname || null],
      patronymic: [savedForm.patronymic || null],
    });
  }

  onclick() {
    this.store.dispatch({ type: 'CLICK' });
  }

  onSetTime() {
    this.store.dispatch({ type: 'TIME' });
  }

  clearForm() {
    localStorage.removeItem('form');
    this.form.reset();
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }

  onSubmit() {
    localStorage.setItem('form', JSON.stringify(this.form.value));
  }
}
